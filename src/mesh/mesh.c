#include "mesh.h"

void buildMesh ( Mesh *m )
{
  unsigned int i, j, k, l;
  Vector s, e;

  /* Alociranje gridova */

  m->grid = XMALLOC( Vector, NUMCELLS * NUMCELLS * NUMCELLS );
  m->bgrid = XMALLOC( unsigned long, NUMCELLS * NUMCELLS * NUMCELLS );

  /* Za sve sto ima veze sa fftw koristim njegovu proceduru za dinamicku alokaciju */
  m->dgrid = (double *) fftw_malloc( sizeof(double) * NUMCELLS * NUMCELLS * NUMCELLS );
  m->pgrid = (double *) fftw_malloc( sizeof(double) * NUMCELLS * NUMCELLS * NUMCELLS );
  /* Napomena: kod fgrid-a se iskoriscava samo NUMCELLS / 2 trece dim. niza */
  m->fgrid = (fftw_complex *) fftw_malloc( sizeof(fftw_complex) * NUMCELLS * NUMCELLS * NUMCELLS );

  if( !m->dgrid || !m->pgrid || !m->fgrid ) {
    fprintf( stderr, "Greska prilikom alokacije memorije sa fftw_malloc\n" );
    exit( 1 );
  }

  m->pforward = fftw_plan_dft_r2c_3d(NUMCELLS, NUMCELLS, NUMCELLS, &(m->dgrid[0]), &(m->fgrid[0]), 0);
  m->pbackward = fftw_plan_dft_c2r_3d(NUMCELLS, NUMCELLS, NUMCELLS, &(m->fgrid[0]), &(m->pgrid[0]), 0);

  /* Dimenzije jedne kockice */
  m->dim = 2 * m->sys->dim / (double) NUMCELLS;

  for( i = 0; i < NUMCELLS; i++ ) {
    for( j = 0; j < NUMCELLS; j++ ) {
      for( k = 0; k < NUMCELLS; k++ ) {
	/* Postavi sve kockice da budu prazne */
	m->bgrid[I(i,j,k)] = 0;
	m->dgrid[I(i,j,k)] = 0.0;

	/* Granice kockica i koordinate cvora */

	/* Pocetak */
	VECT_SET( s, 
		  - m->sys->dim + m->dim * i,
		  - m->sys->dim + m->dim * j,
		  - m->sys->dim + m->dim * k );

	/* Kraj */
	VECT_SET( e,
		  s.x + m->dim,
		  s.y + m->dim,
		  s.z + m->dim );

	/* Centar (cvor) */
	VECT_SET( m->grid[I(i,j,k)], 
		  s.x + (e.x - s.x) / 2,
		  s.y + (e.y - s.y) / 2,
		  s.z + (e.z - s.z) / 2 );	
      }
    }
  }

  /* Dodaj tela u kockice */
  for( l = 0; l < m->sys->bc; l++ ) {
    /* Nadji kojoj kockici telo pripada u zavisnosti od polozaja */
    i = (unsigned int) floor( (m->sys->bodies[l].pos.x + m->sys->dim) / m->dim );
    j = (unsigned int) floor( (m->sys->bodies[l].pos.y + m->sys->dim) / m->dim );
    k = (unsigned int) floor( (m->sys->bodies[l].pos.z + m->sys->dim) / m->dim );

    m->bgrid[I(i,j,k)]++;
    m->dgrid[I(i,j,k)] += m->sys->bodies[l].mass;
  }
}

void calcPotMesh ( Mesh *m )
{
  /* Za sada racuna diskretan potencijal, neophodno je dodati racunanje potencijala na sva tela,
   * kao u slicnoj proceduri za Tree i NBody */

  unsigned int i, j, k;

  const double fac = 1.0 / (double) (NUMCELLS * NUMCELLS * NUMCELLS);

  fftw_execute( m->pforward );
  
  /* Skaliranje */
  for( i = 0; i < NUMCELLS; i++ ) {
    for( j = 0; j < NUMCELLS; j++ ) {
      for( k = 0; k < NUMCELLS / 2 + 1; k++ ) {
        if( i == 0 && j == 0 && k == 0 ) 
	  continue;
	m->fgrid[I(i,j,k)][0] *= (double) (NUMCELLS - 1) * (NUMCELLS - 1) / (i * i + j * j + k * k);
	m->fgrid[I(i,j,k)][1] *= (double) (NUMCELLS - 1) * (NUMCELLS - 1) / (i * i + j * j + k * k);
      }
    }
  }

  fftw_execute( m->pbackward );
  
  for( i = 0; i < NUMCELLS; i++ )
    for( j = 0; j < NUMCELLS; j++ )
      for( k = 0; k < NUMCELLS; k++ )
	m->pgrid[I(i,j,k)] *= fac * G / PI;
}

/* Racuna potencijal u cvorovima kocke brute-force metodom,
 * radi provere */
void calcPotBruteGrid ( Mesh *m )
{
  unsigned int i, j, k;
  unsigned int l;
  double pot;

  FILE *o = fopen( "pot", "w" );
  for( i = 0; i < NUMCELLS; i++ ) {
    for( j = 0; j < NUMCELLS; j++ ) {
      for( k = 0; k < NUMCELLS; k++ ) {
	pot = 0.;
	for( l = 0; l < m->sys->bc; l++ )
	  pot += m->sys->bodies[l].mass / VECT_DIST( m->sys->bodies[l].pos, m->grid[I(i,j,k)] );
	pot *= - G;
	fprintf( o, "%.10lf %.10lf %.10lf %.10lf\n",
		 m->grid[I(i,j,k)].x, m->grid[I(i,j,k)].y, m->grid[I(i,j,k)].z,
		 pot );
      }
    }
  }

  fclose( o );
}
	
