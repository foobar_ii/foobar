/*
    --------------------------------------------------------------------
    LICENSE:
    --------------------------------------------------------------------
    This file is part of Foo
    Copyright 2010 ISPAST Integratori Group
    --------------------------------------------------------------------
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    --------------------------------------------------------------------
    Author: dradojevic
*/

#include "common.h"
#include "foobar-common.h"

/* Podrazumevane vrednosti */

/* Broj tela */
unsigned long N = 5000;

/* Masa sistema */
double M = 1.;

/* Dimenzije sistema */
double D = 30.;

/* Koef. aproksimacije nodova (treecode) */
double K = 1.5;

/* Broj celija za mesh */
unsigned int NUMCELLS = 32;

NBody *mNBody;
Tree *mTree;
Mesh *mMesh;
  
int main (int argc, char **argv)
{ 
  /* enum ApproxMethod mApproxMethod; */

  /* mApproxMethod = TREECODE; */

  /* Tajmeri */
  Timer *mTimer, *NBodyTimer, *TreeTimer, *MeshTimer;
  double bpot, tpot;
  Vector bacc, tacc;

  /* Procitaj opcije/argumente sa komandne linije */
  parseOpt( argc, argv );
  
  mTimer = createTimer();
  
  NBodyTimer = createTimer();
  NBODY_INIT( mNBody, N, D, M );
  printf( "====== NBody ======\n"
	  "Broj tela        : %ld\n"
	  "Dimenzije sistema: %.3lf\n"
	  "Masa sistema     : %.3lf\n"
	  "===================\n", mNBody->bc, mNBody->dim, mNBody->mass );
  printf( "Pripremio NBody za %.6lf s\n", updateTimer( NBodyTimer ) );

  /* Kasnije bismo mogli da u potpunosti sakrijemo drvo, tj. da sve ide preko
   * NBody pokazivaca */

  TreeTimer = createTimer();
  TREE_INIT( mTree, mNBody );
  printf( "====== Stablo ======\n"
	  "Koef. prihvatljivosti: %.3lf\n"
	  "father nodova  : %ld\n"
	  "listova        : %ld\n"
	  "praznih nodova : %ld\n"
	  "====================\n",
	  K, mTree->nc - mTree->lc, mTree->lc, mTree->ec);
  printf( "Pripremio stablo za: %.6lf s\n", updateTimer( TreeTimer ) );

  MeshTimer = createTimer();
  MESH_INIT( mMesh, mNBody );
  printf( "======== Mesh ========\n"
	  "Rezolucija          : %d\n"
	  "Broj kockica/cvorova: %d\n"
	  "======================\n",
	  NUMCELLS, NUMCELLS * NUMCELLS * NUMCELLS );
  printf( "Pripremio mesh za: %.6lf s\n", updateTimer( MeshTimer ) );

  printf( "====== Potencijal ======\n" );
  touchTimer( NBodyTimer );
  bpot = calcPotBrute( mNBody );
  printf( "Izracunao brute pot. za:         %.6lf s\n",
	  updateTimer( NBodyTimer ) );
  
  touchTimer( TreeTimer );
  tpot = calcPotTree( mTree );
  printf( "Izracunao pot. pomocu stabla za: %.6lf s\n",
	  updateTimer( TreeTimer) );

  touchTimer( MeshTimer );
  calcPotMesh( mMesh );
  printf( "Izracunao diskretan pot. pomocu mesh-a za: %.6lf s\n",
	  updateTimer( MeshTimer ) );

  if( NBodyTimer->timer_time - NBodyTimer->last_time > TreeTimer->timer_time - TreeTimer->last_time )
    printf( "Treecode algoritam je bio brzi %.3lf puta.\n",
	  (NBodyTimer->timer_time - NBodyTimer->last_time) /
	  (TreeTimer->timer_time - TreeTimer->last_time) );
  else printf( "Treecode algoritam je bio sporiji %.3lf puta.\n",
	       (TreeTimer->timer_time - TreeTimer->last_time) /
	       (NBodyTimer->timer_time - NBodyTimer->last_time) );
  
  printf( "Brute pot.:      %le\n"
	  "Pot. sa stablom: %le\n", bpot, tpot );
  printf( "Abs greska: +/- %lf\n"
	  "Rel greska: +/- %lf %%\n",
	  fabs( bpot - tpot ), fabs( ( bpot - tpot ) / bpot ) * 100 );

  printf( "====== Ubrzanje ======\n" );
  touchTimer( NBodyTimer );
  bacc = calcAccBrute( mNBody );
  printf( "Izracunao brute acc. za:         %.6lf s\n",
	  updateTimer( NBodyTimer ) );
  touchTimer( TreeTimer );
  tacc = calcAccTree( mTree );
  printf( "Izracunao acc. pomocu stabla za: %.6lf s\n",
	  updateTimer( TreeTimer) );

  if( NBodyTimer->timer_time - NBodyTimer->last_time > TreeTimer->timer_time - TreeTimer->last_time )
    printf( "Treecode algoritam je bio brzi %.3lf puta.\n",
	  (NBodyTimer->timer_time - NBodyTimer->last_time) /
	  (TreeTimer->timer_time - TreeTimer->last_time) );
  else printf( "Treecode algoritam je bio sporiji %.3lf puta.\n",
	       (TreeTimer->timer_time - TreeTimer->last_time) /
	       (NBodyTimer->timer_time - NBodyTimer->last_time) );
  
  printf( "Brute acc.     :" ); VECT_SCRPRT( bacc ); printf( "\n" );
  
  printf( "Acc. sa stablom:" ); VECT_SCRPRT( tacc ); printf( "\n" );

  /* compareField( mMesh ); */

  touchTimer( TreeTimer );
  TREE_FLUSH( mTree );
  updateTimer( TreeTimer );

  touchTimer( MeshTimer );
  MESH_FLUSH( mMesh );
  updateTimer( MeshTimer );
  
  touchTimer( NBodyTimer );
  NBODY_FLUSH( mNBody );
  updateTimer( NBodyTimer );

  printf( "====== Performanse ======\n" );	  
  printf( "Ukupno proteklo vreme      : %.6lf s\n"
	  "(samo operacije sa NBody)  : %.6lf s\n"
	  "(samo operacije sa stablom): %.6lf s\n"
	  "(samo operacije sa mesh-om): %.6lf s\n"
	  "(zbir)                     : %.6lf s\n",
	  updateTimer( mTimer ),
	  NBodyTimer->elapsed_time, TreeTimer->elapsed_time, MeshTimer->elapsed_time,
	  NBodyTimer->elapsed_time + TreeTimer->elapsed_time + MeshTimer->elapsed_time );
  XFREE( mTimer );
  XFREE( NBodyTimer );
  XFREE( TreeTimer );
  XFREE( MeshTimer );

  fgetc( stdin );
  
  return 0;
}
