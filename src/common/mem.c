/*
    --------------------------------------------------------------------
    LICENSE:
    --------------------------------------------------------------------
    This file is part of Foo
    Copyright 2010 ISPAST Integratori Group
    --------------------------------------------------------------------
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    --------------------------------------------------------------------
    Author: dradojevic
*/

#include "common.h"

void * xmalloc (size_t num)
{
  void *n = malloc( num );
  if( !n ) {
    fprintf( stderr, "Memory exhausted" );
    exit( 1 );
  }
  return n;
}

void * xrealloc (void *p, size_t num)
{
  void *n;

  if (!p)
    return xmalloc (num);

  n = realloc (p, num);
  if( !n ) {
    fprintf( stderr, "Memory exhausted" );
    exit( 1 );
  }
  return n;
}

