/*
    --------------------------------------------------------------------
    LICENSE:
    --------------------------------------------------------------------
    This file is part of Foo
    Copyright 2010 ISPAST Integratori Group
    --------------------------------------------------------------------
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    --------------------------------------------------------------------
    Author: dradojevic
*/

#include "vector.h"

unsigned short compareVect ( Vector *v1, Vector *v2 )
{
  if( v1->x > v2->x && v1->y > v2->y && v1->z > v2->z )
    return 0;
  else if( v1->x <= v2->x && v1->y <= v2->y && v1->z <= v2->z )
    return 2;
  else return 1;
}
