/*
    --------------------------------------------------------------------
    LICENSE:
    --------------------------------------------------------------------
    This file is part of Foo
    Copyright 2010 ISPAST Integratori Group
    --------------------------------------------------------------------
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    --------------------------------------------------------------------
    Author: dradojevic
*/

#include "common.h"
#include "args.h"

void parseOpt (int argc, char **argv )
{
  int c;

  while( 1 ) {
    c = getopt( argc, argv, "n:k:d:m:c:" );
  
    if( c == -1 )
      break;

    switch( c ) {
    case 'n':
      N = atoi( optarg );
      if( !N ) {
	fprintf( stderr, 
		 "Broj tela mora biti veci od nule (ili greska pri citanju broja tela)\n");
	exit( 1 );
      }
      break;
   
    case 'c':
      NUMCELLS = atoi( optarg );
      if( !NUMCELLS || NUMCELLS % 2 != 0 ) {
	fprintf( stderr, 
		 "Rezolucija mesh-a mora biti parna i veca od nule (ili greska pri citanju rezolucije mesh-a)\n");
	exit( 1 );
      }
      break;

    case 'k':
      K = atof( optarg );
      if( K == 0.0 ) {
	fprintf( stderr, 
		 "Greska pri citanju koeficijenta aproksimacije\n");
	exit( 1 );
      }
      break;

    case 'm':
      M = atof( optarg );
      if( M == 0.0 ) {
	fprintf( stderr, 
		 "Greska pri citanju mase sistema\n");
	exit( 1 );
      }
      break;

    case 'd':
      D = atof( optarg );
      if( D == 0.0 ) {
	fprintf( stderr, 
		 "Greska pri citanju dimenzija sistema\n");
	exit( 1 );
      }
      break;
    case '?':
      /* Nepoznata opcija */
      break;
    
    default:
      fprintf( stderr, "Greska prilikom citanja opcija sa komandne linije: 0%o\n", c );
      exit( 1 );
    }

  }
  
  /* Argumenti (nisu opcije) */
  /* if( optind < argc ) { */
  /*   printf( "Argumenti: " ); */
  /*   while (optind < argc) */
  /*     printf ("%s ", argv[optind++]); */
  /*   printf ("\n"); */
  /* } */
}
