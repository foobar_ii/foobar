/*
    --------------------------------------------------------------------
    LICENSE:
    --------------------------------------------------------------------
    This file is part of Foo
    Copyright 2010 ISPAST Integratori Group
    --------------------------------------------------------------------
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    --------------------------------------------------------------------
    Author: dradojevic
*/

#include "common.h"
#include "rnd.h"

float ran2 ( long *idum ) 
{
  int j;
  long k;

  static long idum2 = 123456789;
  static long iy = 0;
  static long iv[NTAB];

  float temp;

  if( *idum <= 0 ) {
    if( -(*idum) < 1 )
      *idum = 1;
    else *idum = -(*idum);
 		
    idum2 = (*idum);
    for( j = NTAB + 7; j >= 0; j-- ) {
      k = (*idum) / IQ1;
      *idum = IA1 * (*idum - k * IQ1) - k * IR1;
      if ( *idum < 0 )
	*idum += IM1;
      if ( j < NTAB ) 
	iv[j] = *idum;
    }
    iy = iv[0];
  }
  k = *idum / IQ1;
  *idum = IA1 * (*idum - k * IQ1) - k * IR1;
  if( *idum < 0 )
    *idum += IM1;
  k = idum2 / IQ2;
  idum2 = IA2 * (idum2 - k * IQ2) - k * IR2;
  if( idum2 < 0 )
    idum2 += IM2;
  j = iy / NDIV;
  iy = iv[j] - idum2;
  iv[j] = *idum;
  if( iy < 1 )
    iy += IMM1;
  if( (temp = AM * iy) > RNMX )
    return RNMX;
  else return temp;
}

double genRnd ( long* idum, double min, double max ) {
  return min + ran2(idum) * (max - min);
}

void spherical ( Vector *v, unsigned short type, double r, double dim ) 
{
  static long a = 5;
  
  double theta;
  double phi;
  
  theta = acos( genRnd( &a, -1, 1 ) );
  phi = genRnd( &a, 0, 2 * PI );
		
  v->x = r * sin( theta ) * cos( phi );
  v->y = r * sin( theta ) * sin( phi );
  v->z = r * cos( theta );
		
  if( !type ) {
    double dr = 0;
    /* Korekcija koordinata tako da budu unutar sistema 
     * (valjda :)) */
    while( VECT_ABS( *v ) > dim / 2 ) { 					
      dr += r / 1000;
      theta = acos( genRnd( &a, -1, 1 ) );
      phi = genRnd( &a, 0, 2 * PI );
      
      v->x = (r - dr) * sin( theta ) * cos( phi );
      v->y = (r - dr) * sin( theta ) * sin( phi );
      v->z = (r - dr) * cos( theta );
    }
  }
}
