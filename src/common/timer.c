/*
    --------------------------------------------------------------------
    LICENSE:
    --------------------------------------------------------------------
    This file is part of Foo
    Copyright 2010 ISPAST Integratori Group
    --------------------------------------------------------------------
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    --------------------------------------------------------------------
    Author: dradojevic
*/

#include "common.h"
#include "timer.h"

/* Ovo je iskucano u roku od nekoliko
 * minuta, previse je sloppy i glupo,
 * ako neko ima vremena neka sredi malo */

/* Mrzim sto nemamo konstruktore :) */
Timer * createTimer( void )
{
  Timer *t = XMALLOC( Timer, 1 );
  t->elapsed_time = 0;
  t->start_time = getTime();
  t->timer_time = t->start_time;
  return t;
}

void touchTimer( Timer *t )
{
  t->timer_time = getTime();
}

double updateTimer( Timer *t )
{
  t->last_time = t->timer_time;
  t->timer_time = getTime();
  t->elapsed_time += t->timer_time - t->last_time;
  
  return t->timer_time - t->last_time;
}

double getTime (void)
{
  struct timeval read_time;
  double this_time;
  
  gettimeofday( &read_time, 0 );
  this_time = (double) read_time.tv_sec + (double) read_time.tv_usec / 1000000;

  return this_time;
}
