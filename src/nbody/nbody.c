/*
    --------------------------------------------------------------------
    LICENSE:
    --------------------------------------------------------------------
    This file is part of Foo
    Copyright 2010 ISPAST Integratori Group
    --------------------------------------------------------------------
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    --------------------------------------------------------------------
    Author: dradojevic
*/

#include "common.h"
#include "rnd.h"
#include "nbody.h"

NBody * makeNBody ( unsigned long bc, double dim, double mass )
{
  NBody *n = XMALLOC( NBody, 1 );
  
  n->bc = bc;
  n->dim = dim;
  n->mass = mass;
  
  n->bodies = XMALLOC( Body, n->bc );
  genPlummer( n );
 
  /* Sledece uzima previse vremena, iskomentarisacu dok ne bude
   * potrebno */
  /* n->ek = calcEk( n ); */
  /* n->ep = calcEp( n ); */
  n->ek = 0.0;
  n->ep = 0.0;

  return n;
}

void shiftToCM ( NBody *n )
{
  unsigned long i;
  Vector cm, vm, temp;
  double eps = 0.00005;
  
  VECT_NULLIFY( cm );
  VECT_NULLIFY( vm );

  for( i = 0; i < n->bc; i++ ) {
    VECT_SMUL( temp, n->bodies[i].pos, n->bodies[i].mass );
    VECT_ADD( cm, cm, temp );
    VECT_SMUL( temp, n->bodies[i].vel, n->bodies[i].mass );
    VECT_ADD( vm, vm, temp );
  }
	
  VECT_SMUL( cm, cm, 1. / n->mass );
  VECT_SMUL( vm, vm, 1. / n->mass );

  for( i = 0; i < n->bc; i++ ) {
    VECT_SUB( n->bodies[i].pos, n->bodies[i].pos, cm );
    VECT_SUB( n->bodies[i].vel, n->bodies[i].vel, vm );
  }														
  
  VECT_NULLIFY( cm );
  VECT_NULLIFY( vm );
 	
  for( i = 0; i < n->bc; i++ ) {
    VECT_SMUL( temp, n->bodies[i].pos, n->bodies[i].mass );
    VECT_ADD( cm, cm, temp );
    VECT_SMUL( temp, n->bodies[i].vel, n->bodies[i].mass );
    VECT_ADD( vm, vm, temp );
  }
  
  VECT_SMUL( cm, cm, 1./n->mass );
  VECT_SMUL( vm, vm, 1./n->mass );
  
  if( VECT_ABS( cm ) < 0 + eps && VECT_ABS( cm ) > 0 - eps ) {
    if( !(VECT_ABS( vm ) < 0 + eps && VECT_ABS( vm ) > 0 - eps) ) {
      fprintf( stderr, "CM nije dobro izracunat\n" );
      exit (1);
    }
  }
}

void adjUnits ( NBody *n ) 
{
  unsigned long i;
  double scale = fabs( n->ep ) / (2 * n->ek);
	
  for( i = 0; i < n->bc; i++ )
    VECT_SMUL( n->bodies[i].vel, n->bodies[i].vel, sqrt( scale ) );
}

double calcPotBrute ( NBody *n )
{
  unsigned long i, j;
  double pot = 0.0;
  
  for( i = 0; i < n->bc; i++ ) {
    for( j = 0; j < n->bc; j++ ) {
      if( j != i )
	pot += n->bodies[j].mass / VECT_DIST( n->bodies[i].pos, n->bodies[j].pos );
    }
  }
  return - G * pot;
}

Vector calcAccBrute ( NBody *n )
{
  unsigned long i, j;

  double r3;
  Vector a, r, v;

  /* Softening parametar, regulise bliske prolaze */
  const double eps = 5e-6;

  VECT_NULLIFY( a );
  for( i = 0; i < n->bc; i++ ) {
    for( j = 0; j < n->bc; j++) {
      if( j != i ) {
	VECT_SUB( r, n->bodies[i].pos, n->bodies[j].pos );
	r3 = VECT_ABS( r ) * VECT_ABS( r ) * VECT_ABS( r );
	VECT_SMUL( v, r, - G * n->bodies[j].mass / (r3 + eps) );
	VECT_ADD( a, a, v );
      }
    }
  }

  return a;
}

double calcBodyEp ( NBody *n, Body *b ) 
{
  unsigned long i;
  double ep = 0.0; 	       

  for( i = 0; i < n->bc; i++ )
    if( &(n->bodies[i]) != b )
      ep += - G * n->bodies[i].mass * b->mass /  VECT_DIST( n->bodies[i].pos, b->pos );
  
  return ep;
}

double calcEp ( NBody *n ) 
{
  unsigned long i;
  double ep = 0.0;
 	
  for( i = 0; i < n->bc; i++ )
    ep += calcBodyEp( n, &(n->bodies[i]) );
                     
  return ep / 2.0;
}

double calcBodyEk ( Body *b ) 
{
  return b->mass * VECT_ABS( b->vel ) * VECT_ABS( b->vel ) * 0.5;
}

double calcEk ( NBody *n ) 
{
  unsigned long i;
  double ek = 0.0;

  for( i = 0; i < n->bc; i++ )
    ek += calcBodyEk( &(n->bodies[i]) );
  
  return ek;
}

void genPlummer ( NBody *n )
{
  unsigned long i;

  /* Seed */
  long a = (long) -(time( NULL ));
  
  /* Ne znam sta rade, ali su potrebne */
  double radius, q, g;

  double max_dist = 0.0;
  /* Random generator se inicijalizuje samo jednom, i to
   * za negativno a */
  ran2( &a );
  /* Spreci reinicijalizaciju seed-a */
  a = 5;
  
  for ( i = 0; i < n->bc; i++ ) {
    /* Uniformna raspodela mase */
    n->bodies[i].mass = n->mass / n->bc;
    
    /* Koordinate */
    radius = ran2( &a );
    radius = 1.0 / sqrt( pow( radius, -2.0/3.0 ) - 1.0 );
    
    spherical( &(n->bodies[i].pos), 0, radius, n->dim );
  
    /* Stvarno ne znam cemu sluzi ovo dole... */
    VECT_SMUL( n->bodies[i].pos, n->bodies[i].pos, 1./0.91 );

    if( VECT_ABS( n->bodies[i].pos) > max_dist )
      max_dist = VECT_ABS( n->bodies[i].pos);

    q = 0.0;
    g = 0.1;
    
    while( g > q * q * pow( (1.0 - q * q), 3.5 ) ) {
      q = ran2( &a );
      g = ran2( &a ) * 0.1;
    }
    
    radius = q * sqrt( 2.0 ) * pow( (1.0 + radius * radius ), -0.25);
    spherical( &(n->bodies[i].vel), 1, radius, n->dim );
  }

  /* Povecaj malo za svaki slucaj (necemo da smanjujemo dimenzije bezveze) */
  /* n->dim = max_dist * 1.2; */
}

