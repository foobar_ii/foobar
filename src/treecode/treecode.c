/*
    --------------------------------------------------------------------
    LICENSE:
    --------------------------------------------------------------------
    This file is part of Foo
    Copyright 2010 ISPAST Integratori Group
    --------------------------------------------------------------------
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    --------------------------------------------------------------------
    Author: dradojevic
*/

#include "common.h"
#include "treecode.h"

Node * makeNode ( Vector *start, Vector* end, unsigned long level, 
		 unsigned long ord, unsigned short npos, unsigned long bc ) 
{
  unsigned short i;
  Node *n;

  n = XMALLOC( Node, 1 );
  n->bodies = XMALLOC( Body *, bc );
	
  VECT_CPY( n->start, *start  );
  VECT_CPY( n->end, *end );
	
  /* Izracunaj geom. centar */
  VECT_SET( n->center, fabs( (start->x + end->x) / 2 ), fabs( (start->y + end->y) / 2 ),
	    fabs( (start->z + end->z) / 2 ) );
  
  n->level = level;
  n->ord   = ord;
  n->npos  = npos;
  
  n->bc = 0;
  n->dc = 0;
  
  /* Default tip */
  n->type = FATHER;
 	
  n->prev = 0;
  n->next = 0;
  n->father = 0;

  for( i = 0; i < 8; i++ )
    n->daughters[i] = 0;
 	
  n->mono = 0;	
  for( i = 0; i < 6; i++ )
    n->quad[i] = 0;

  return n;
}

void calcNodeCM ( Node *n )
{
  unsigned long i;
  Vector com, temp;
  VECT_NULLIFY( com );
  
  for ( i = 0; i < n->bc; i++ ) {
    VECT_SMUL( temp, n->bodies[i]->pos, n->bodies[i]->mass );
    VECT_ADD( com, com, temp );
  }
  VECT_SMUL( com, com, 1./n->mono);
  
  VECT_CPY( n->cm, com );
}

/* ako/kad budem radio izgradnju stabla rekurzivno... */
/* Prvi poziv: if( recBuildTree( mTree, start, end, 0, 0, 0, mTree->sys->bc, 0, 0, 0) == -1 )... */
/* -1 - sranje
 * 0  - dalje
 * 1  - gotov */
/* short rcsBuildTree ( Tree *t, Vector *from, Vector *to, unsigned long level, */
/* 		     unsigned long ord, unsigned short npos, unsigned long bc, */
/* 		     unsigned long buff, */
/* 		     Node *father, Node *prev, Node *last) { */
  
  
void buildTree ( Tree *t )
{
  /* Ogranicavajuci vektori, from - donji levi ugao, to - gornji desni ugao,
   * definisu granice drveta, obuhvataju ceo sistem */
  Vector to, from;

  /* Centar kockice */
  Vector cs;

  /* Prethodni nod */
  Node *nd_prev;

  /* Nod poslednjeg grananja, na pocetku je to root nod */
  Node *nd_branched;

  /* Radijus jedne kockice */
  double sdim = t->sys->dim;

  unsigned long i;

  /* Koliko mesta za nodove je rezervisano, rezervisi pocetni bafer */
  unsigned long buff = 0;

  /* Trenutni dubinski nivo */
  unsigned long level = 0;
  
  /* Brojac nodova */
  unsigned long ord = 0;

  /* Pozicioni marker, od 0 do 8, 0 je root */
  short npos = -1;

  /* Tela u nodu, moraju se prebrojati prvo */
  unsigned long bodies;
  
  VECT_SET( from, - sdim, - sdim, - sdim );
  VECT_SET( to, sdim, sdim, sdim );

  /* Root */
  nd_branched = makeNode( &from, &to, 0, 0, 0, t->sys->bc );
  for( i = 0; i < t->sys->bc; i++ )
    ND_ADDBODY( nd_branched, &(t->sys->bodies[i]) );
  
  nd_prev = nd_branched;

  while( 1 ) {
    npos++;
    bodies = 0;
    for( i = 0; i < nd_branched->dc; i++ )
      bodies += nd_branched->daughters[i]->bc;
    if( bodies == nd_branched->bc ) {
      /* Ostali nodovi na grani su prazni, odmah predji na zadnji */
      t->ec += 8 - npos;
      npos = 8;
    }
    bodies = 0;

    /* Odredjivanje granica */
    VECT_SET( cs, (nd_branched->start.x + nd_branched->end.x) / 2, (nd_branched->start.y + nd_branched->end.y) / 2,
	      (nd_branched->start.z + nd_branched->end.z) / 2 );
    sdim = t->sys->dim / pow( 2, level - 1 );
    
    switch( npos ) {
    case 0:
      break;
    case 1:
      VECT_SET( from, cs.x - sdim, cs.y - sdim, cs.z - sdim );
      VECT_SET( to, cs.x,cs.y,cs.z );
      break;    				
    case 2:
      VECT_SET( from, cs.x, cs.y, cs.z - sdim );
      VECT_SET( to, cs.x + sdim, cs.y + sdim, cs.z );
      break;    				
    case 3:
      VECT_SET( from, cs.x - sdim, cs.y, cs.z - sdim );
      VECT_SET( to, cs.x, cs.y + sdim, cs.z );
      break;    				
    case 4:
      VECT_SET( from, cs.x, cs.y - sdim, cs.z - sdim );
      VECT_SET( to, cs.x + sdim, cs.y, cs.z );
      break;    				
    case 5:
      VECT_SET( from, cs.x - sdim, cs.y - sdim, cs.z );
      VECT_SET( to, cs.x, cs.y, cs.z + sdim );
      break;    				
    case 6:
      VECT_SET( from, cs.x, cs.y, cs.z );
      VECT_SET( to, cs.x + sdim, cs.y + sdim, cs.z + sdim );
      break;    				
    case 7:
      VECT_SET( from, cs.x - sdim, cs.y, cs.z );
      VECT_SET( to, cs.x, cs.y + sdim, cs.z + sdim );
      break;    				
    case 8:
      VECT_SET( from, cs.x, cs.y - sdim, cs.z );
      VECT_SET( to, cs.x + sdim, cs.y, cs.z + sdim );
      break;
				
    default:
      fprintf( stderr, "Greska pri podeli prostora na kockice\n" );
      exit( 1 );
    }

    /* Prebrajanje */
    for( i = 0; i < nd_branched->bc; i++ ) {
      if( compareVect( &(nd_branched->bodies[i]->pos), &to ) == 2 &&
	  compareVect( &(nd_branched->bodies[i]->pos), &from) == 0 )
	bodies++;
    }
    if( bodies > 0 ) {
      /* Dodaj mesta ako treba */
      if( ord + 1 >= buff ) {
	buff += ND_BUFF;
	t->nodes = XREALLOC( Node *, t->nodes, buff );
      }
      /* Napravi nod */
      if( nd_prev && !nd_prev->ord ) {
	/* Root, samo povezi */
	t->nodes[ord] = nd_prev;
	nd_prev = 0;

	level = 1;
	ord = 1;
	t->levels = 1;
	continue;
      }
      else {
	t->nodes[ord] = makeNode( &from, &to, level, ord, npos, bodies );
	
	/* Promeni pokazivac na poslednjeg */
	nd_prev = t->nodes[ord];
	
	t->nc++;

	/* Dodaj tela */
	for( i = 0; i < nd_branched->bc; i++ ) { 
	  if( compareVect( &(nd_branched->bodies[i]->pos), &to) == 2 && 
	      compareVect( &(nd_branched->bodies[i]->pos), &from) == 0 ) {
	    ND_ADDBODY( t->nodes[ord], nd_branched->bodies[i]);
	    t->nodes[ord]->mono += nd_branched->bodies[i]->mass;
	  }
	}
	
	/* Rodbinske veze */
	
	/* Otac, cerka */
	ND_SETFATHER( t->nodes[ord], nd_branched );	
	ND_SETDAUGHTER( t->nodes[ord]->father, t->nodes[ord] );

	/* Prethodni, sledeci */
	/* if( t->nodes[ord_curr - 1]->level == level && npos - 1 != 1 ) { */
	if( t->nodes[ord - 1]->level == level ) {
	  /* Prethodni nod se nalazi na trenutnoj grani */
	  ND_SETPREV( t->nodes[ord], t->nodes[ord - 1] );
	  ND_SETNEXT( t->nodes[ord - 1], t->nodes[ord] );
	}
	/* else if( t->nodes[ord_curr - 1]->level > level && npos - 1 != 1 ) { */
	else if( t->nodes[ord - 1]->level > level ) {
	  /* Prethodni nod se nalazi na grani ispod, trenutni nod nije prvi na grani */
	  for( i = nd_branched->dc - 2; i >= 0; i-- ) {
	    /* Proveri sve cerke, polazeci od najmladje, ne racunajuci tren. nod :) */
	    if( nd_branched->daughters[i] ) {
	      ND_SETPREV( t->nodes[ord], nd_branched->daughters[i] );
	      ND_SETNEXT( nd_branched->daughters[i], t->nodes[ord] );
	      break;
	    }
	  }
	}
      
	if( bodies == 1 ) {
	  /* List */
	  t->nodes[ord]->type = LEAF;
	  t->nodes[ord]->bodies[0]->father = t->nodes[ord];
	  t->lc++;
	  
	  t->nodes[ord]->cm = t->nodes[ord]->bodies[0]->pos;
	} /* list */
	else {
	  /* Father */
	  calcNodeCM( t->nodes[ord] );
	  /* t->nodes[ord]->type = FATHER; - default */
	  for( i = 0; i < bodies; i++ ) {
	    /* Iskoristicu cs ponovo da ne bih pravio novi */
	    VECT_SUB( cs, t->nodes[ord]->bodies[i]->pos, t->nodes[ord]->cm );
	    
	    t->nodes[ord]->quad[0] += t->nodes[ord]->bodies[i]->mass
	      * (3 * cs.x * cs.x - VECT_ABS( cs ) * VECT_ABS( cs )); /*  * deltaF(k,j) = 1 */
	    t->nodes[ord]->quad[1] += t->nodes[ord]->bodies[i]->mass
	      * (3 * cs.y * cs.y - VECT_ABS( cs ) * VECT_ABS( cs )); /*  * deltaF(k,j) = 1 */
	    t->nodes[ord]->quad[2] += t->nodes[ord]->bodies[i]->mass
	      * (3 * cs.z * cs.z - VECT_ABS( cs ) * VECT_ABS( cs )); /*  * deltaF(k,j) = 1 */
	    t->nodes[ord]->quad[3] += t->nodes[ord]->bodies[i]->mass * 3 * cs.x * cs.y;
	    t->nodes[ord]->quad[4] += t->nodes[ord]->bodies[i]->mass * 3 * cs.x * cs.z;
	    t->nodes[ord]->quad[5] += t->nodes[ord]->bodies[i]->mass * 3 * cs.y * cs.z;
	  }
	}/* father */
      } /* !root */
    } /* bodies > 0 */
    else {
      /* Prazan */
      t->ec++;
      nd_prev = 0;
    }
    
    if( nd_prev && nd_prev->type == FATHER ) {
      /* Grananje */
      nd_branched = t->nodes[ord];
      level++;
      if( level > t->levels )
	t->levels++;
      ord++;
      npos = 0; /* zato sto se povecava na pocetku petlje */
    }
    else {
      /* List ili empty*/
      if( npos == 8 ) {
	/* Kraj grane */	  
	while( nd_branched->npos == 8 )
	  /* Dok je mesto poslednjeg grananja na kraju te grane */
	  nd_branched = nd_branched->father;
	  
	if ( !nd_branched->ord )
	  /* Stigli do root-a, kraj */
	  break;			
	else {
	  /* Vratili se na granu na kojoj jos nismo dosli do kraja */
	  level = nd_branched->level;
	  npos = nd_branched->npos;
	  nd_branched = nd_branched->father;
	}
      } /* kraj grane */
      if( nd_prev )
	/* List */
	ord++;
    }
  } /* while( 1 ) */
}      

unsigned long scanTreeVector ( Tree *t, Vector *p, Node *valid[] )
{
  unsigned long i;

  /* Brojac tela, za proveru */
  unsigned long bc = 0;

  /* Iterator */
  Node *nd_curr = 0;

  /* Broj pogodnih nodova */
  unsigned long vc = 0;
  
  /* Nadji prvu cerku */
  for( i = 0; i < 8; i++ ) {
    if( t->nodes[0]->daughters[i] ) {
      nd_curr = t->nodes[0]->daughters[i];
      break;
    }
  }
  if( !nd_curr ) {
    fprintf( stderr, "Greska pri hodanju po stablu\n" );
    exit( 1 );
  }

  while( 1 ) {
    if( nd_curr->type == FATHER ) {
      if( VECT_DIST( *p, nd_curr->cm ) >= K * VECT_DIST( nd_curr->end, nd_curr->start ) ) {
	valid[vc] = nd_curr;
	bc += valid[vc]->bc;
	vc++;
      }
      else {
	/* Trenutni nije zadovoljio kriterijum, i trenutni je father, granaj */
	nd_curr = t->nodes[nd_curr->ord + 1];
	continue;
      }
    }
    else {
      /* List, odmah dodaj */
      valid[vc] = nd_curr;
      bc++;
      vc++;
    }
    
    while( !nd_curr->next ) {    
      nd_curr = nd_curr->father;
      if( !nd_curr->ord ) {
	/* Root */
	if( bc != t->sys->bc ) {
	  fprintf( stderr, "Greska pri hodanju po stablu\n" );
	  exit( 1 );
	}
	return vc;
      }
    }
    nd_curr = nd_curr->next;
  }
}

unsigned long scanTreeBody ( Tree *t, Body *b, Node *valid[] )
{
  unsigned long i;

  /* Brojac tela, za proveru */
  unsigned long bc = 0;

  /* Iterator */
  Node *nd_curr = 0;

  /* Broj pogodnih nodova */
  unsigned long vc = 0;
  
  /* Nadji prvu cerku */
  for( i = 0; i < 8; i++ ) {
    if( t->nodes[0]->daughters[i] ) {
      nd_curr = t->nodes[0]->daughters[i];
      break;
    }
  }
  if( !nd_curr ) {
    fprintf( stderr, "Greska pri hodanju po stablu\n" );
    exit( 1 );
  }

  while( 1 ) {
    /* printf( "%ld [%ld] {%d}\n", nd_curr->level, nd_curr->ord, nd_curr->type ); */
    if( nd_curr->type == FATHER ) {
      if( VECT_DIST( b->pos, nd_curr->cm ) >= K * VECT_DIST( nd_curr->end, nd_curr->start ) ) {
	valid[vc] = nd_curr;
	bc += valid[vc]->bc;
	vc++;
      }
      else {
	/* Trenutni nije zadovoljio kriterijum, i trenutni je father, granaj */
	nd_curr = t->nodes[nd_curr->ord + 1];
	continue;
      }
    }
    else if( nd_curr != b->father ) {
      /* List, odmah dodaj */
      valid[vc] = nd_curr;
      bc++;
      vc++;
    }

    while( !nd_curr->next ) {    
      nd_curr = nd_curr->father;
      if( !nd_curr->ord ) {
	/* Root */
	/* Ako broj tela nije jednak ukupnom broju - 1 (list je preskocen)
	 * ili ako broj tela nije jednak ukupnom broju (ako se list za koji
	 * se pot. racuna nalazi u father nodu koji je prihvacen) */
	if( !(bc == t->sys->bc - 1 || bc == t->sys->bc) ) {
	  fprintf( stderr, "Greska pri hodanju po stablu\n"
		           "BC je %ld\n", bc );
	  exit( 1 );
	}
	return vc;
      }
    }
    nd_curr = nd_curr->next;
  }
}

double calcPotTree ( Tree *t )
{
  unsigned long i, j;
  double dist, r5, phi;

  Vector r;
  
  Node **bin = XMALLOC( Node *, t->nc );

  unsigned long vc;
  
  phi = 0.0;
  for( j = 0; j < t->sys->bc; j++ ) {
    vc = scanTreeBody( t, &(t->sys->bodies[j]), bin );
    
    /* printf( "Ukupno %ld nodova doprinose potencijalu.\n", vc ); */
    for( i = 0; i < vc; i++ ) {
      VECT_SUB( r, t->sys->bodies[j].pos, bin[i]->cm );
      dist = VECT_ABS( r );
      
      if ( bin[i]->type == FATHER ) {
	/* Koristi kvadrupolni mom */
	r5 = dist * dist * dist * dist * dist;
	phi += - bin[i]->mono / dist
	  - bin[i]->quad[0] * r.x * r.x / 2 / r5
	  - bin[i]->quad[1] * r.y * r.y / 2 / r5
	  - bin[i]->quad[2] * r.z * r.z / 2 / r5
	  - bin[i]->quad[3] * r.x * r.y / 2 / r5
	  - bin[i]->quad[4] * r.x * r.z / 2 / r5
	  - bin[i]->quad[5] * r.y * r.z / 2 / r5;
      }
      else phi += - bin[i]->mono / dist;
    }
  }

  XFREE( bin );
  return phi;
}

Vector calcAccTree ( Tree * t )
{
  unsigned long i, j;
  unsigned long vc;

  Vector a, r, v;

  double dist, r3, r7, tmp;
  Node **bin = XMALLOC( Node *, t->nc );
  
  const double eps = 5e-6;

  VECT_NULLIFY( a );
  for( i = 0; i < t->sys->bc; i++ ) {
    vc = scanTreeBody( t, &(t->sys->bodies[i]), bin );
    
    for( j = 0; j < vc; j++ ) {
      VECT_SUB( r, t->sys->bodies[i].pos, bin[j]->cm );
      dist = VECT_ABS( r );
      r3 = dist * dist * dist;

      if( bin[j]->type == FATHER ) {
	r7 = r3 * r3 * dist;

	tmp = r.x * r.x * bin[j]->quad[0] 
	  + r.y * r.y * bin[j]->quad[1] 
	  + r.z * r.z * bin[j]->quad[2] 
	  + 2 * r.x * r.y * bin[j]->quad[3] 
	  + 2 * r.x * r.z * bin[j]->quad[4] 
	  + 2 * r.y * r.z * bin[j]->quad[5];

	v.x = - G * r.x * bin[j]->mono / r3
	  - G * (5 * tmp * r.x - 2 * dist * dist * (bin[j]->quad[0] * r.x + bin[j]->quad[3] * r.y + bin[j]->quad[4] * r.z)) 
	  / 2.0 / r7;
	
	v.y = - G * r.y * bin[j]->mono / r3
	  - G * (5 * tmp * r.y - 2 * dist * dist * (bin[j]->quad[1] * r.y + bin[j]->quad[3] * r.x + bin[j]->quad[5] * r.z))
	  / 2.0 / r7;

	v.z = - G * r.z * bin[j]->mono / r3
	  - G * (5 * tmp * r.z - 2 * dist * dist * (bin[j]->quad[2] * r.z + bin[j]->quad[5] * r.y + bin[j]->quad[4] * r.x)) 
	  / 2.0 / r7;
      }
      else VECT_SMUL( v, r, - G * bin[j]->mono / (r3 + eps) );
      
      VECT_ADD( a, a, v );
    }
  }

  XFREE( bin );
  return a;
}

void flushTree ( Tree *t )
{
  unsigned long i;
  for( i = 0; i <= t->nc; i++ ) {
    XFREE( t->nodes[i]->bodies );
    XFREE( t->nodes[i] );
  }
  XFREE( t->nodes );
  XFREE( t );
}
