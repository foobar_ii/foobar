/*
    --------------------------------------------------------------------
    LICENSE:
    --------------------------------------------------------------------
    This file is part of Foo
    Copyright 2010 ISPAST Integratori Group
    --------------------------------------------------------------------
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    --------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __BODY_H__
#define __BODY_H__

#include "vector.h"

#define NBODY_INIT(np,n,d,m)			\
  {						\
    (np) = makeNBody( (n), (d), (m) );		\
    shiftToCM( (np) );				\
  }

#define NBODY_FLUSH(np)				\
  {						\
    XFREE( (np)->bodies );			\
    XFREE( (np) );				\
  }

typedef struct Body
{
  Vector pos, vel;
  double mass;

  /* Pokazivac na strukturu u kojoj se telo nalazi */
  void *father;
} Body;

typedef struct NBody
{
  /* Broj tela */
  unsigned long bc;
  
  /* Ukupna masa sistema */
  double mass;

  /* Kineticka i potencijalna energija sistema */
  double ek, ep;

  /* Maksimalna dimenzija sistema, drvo/mreza se gradi od 0 do
   * +/- dim po sve tri ose */
  double dim;

  /* Centar mase sistema */
  /* Vector cm; - nepotrebno, bice priblizno 0 nakon 
   * shiftToCM */

  /* Pokazivac na strukturu koja se gradi na sistemu */
  void *base;

  Body *bodies;	
} NBody;

extern NBody * makeNBody( unsigned long bc, double dim, double mass );

extern void shiftToCM( NBody *n );

/* Za sada se nigde ne koristi, normalizuje brzine tela */
extern void adjUnits( NBody *n );

extern double calcBodyEp( NBody *n, Body *b );
extern double calcEp( NBody *n );

extern double calcBodyEk( Body *b );
extern double calcEk( NBody *n );

extern double calcPotBrute( NBody *n );

extern Vector calcAccBrute( NBody *n );

extern void genPlummer( NBody *n );

#endif
