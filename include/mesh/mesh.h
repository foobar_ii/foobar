#ifndef __MESH_H__
#define __MESH_H__

#include "common.h"
#include "vector.h"
#include "nbody.h"

#include <fftw3.h>

#define MESH_INIT(mp,np)						\
  {									\
    (mp) = XMALLOC( Mesh, 1 );						\
    (mp)->sys   = (np);							\
    (np)->base  = (mp);							\
    buildMesh( (mp) );							\
  }

#define MESH_FLUSH(mp)							\
  {									\
    XFREE( (mp)->grid );						\
    XFREE( (mp)->bgrid );						\
    fftw_free( (mp)->dgrid );						\
    fftw_free( (mp)->pgrid );						\
    fftw_free( (mp)->fgrid );						\
    fftw_destroy_plan( (mp)->pforward );				\
    fftw_destroy_plan( (mp)->pbackward );				\
    fftw_cleanup();							\
    XFREE( (mp) );							\
  }

/* Prvi i drugi indeks uvek idu do NUMCELLS */
#define I(i,j,k) ((k) + NUMCELLS * ((j) + NUMCELLS * (i)))

typedef struct Mesh
{
  /* Dimenzije kockice, dx, dy, dz */
  double dim;

  /* NAPOMENA: fftw zahteva specificnu alokaciju n-dimenzionalnih nizova, tj,
   * n-to dimenzionalni niz se interpretira kao jednodimenzionalni, pa se indeksiranje
   * vrsi na drugi nacin: indeks [i][j][k] postaje [k + j * (DIM2 + i * DIM1)] */

  /* Koordinate cvorova */
  Vector *grid;

  /* Broj tela u kockicama */
  unsigned long *bgrid;

  /* Density grid */
  double *dgrid;

  /* Potential grid */
  double *pgrid;

  /* Medjuproizvod */
  fftw_complex *fgrid;

  /* Sistem N tela na osnovu koga se gradi mesh */
  NBody *sys;

  fftw_plan pforward, pbackward;
} Mesh;

extern void buildMesh( Mesh *m );
extern void calcPotMesh( Mesh *m );
/* nedostaju procedure za izracunavanje potencijala proizvoljnog tela preko mesha 
 * kao i ubrzanja na telo preko mesha (treba raditi interpolacijom) */
extern void calcPotBruteGrid( Mesh *m );

#endif
