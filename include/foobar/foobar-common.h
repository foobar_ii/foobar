/*
    --------------------------------------------------------------------
    LICENSE:
    --------------------------------------------------------------------
    This file is part of Foo
    Copyright 2010 ISPAST Integratori Group
    --------------------------------------------------------------------
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    --------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __FOO_COMMON_H__
#define __FOO_COMMON_H__

#include "args.h"
#include "rnd.h"
#include "timer.h"
#include "nbody.h"
#include "treecode.h"
#include "mesh.h"

/* Metod aproksimacije */
/* extern enum ApproxMethod mApproxMethod; */

extern NBody *mNBody;
extern Tree *mTree;
extern Mesh *mMesh;
 
#endif
