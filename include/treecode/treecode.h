/*
    --------------------------------------------------------------------
    LICENSE:
    --------------------------------------------------------------------
    This file is part of Foo
    Copyright 2010 ISPAST Integratori Group
    --------------------------------------------------------------------
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    --------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __TREECODE_H__
#define __TREECODE_H__

#include "vector.h"
#include "nbody.h"

/* Broj Node * za alociranje odjednom */
#define ND_BUFF 100000

enum NodeType
  {
    FATHER,
    LEAF,
    EMPTY /* samo kod buildTree() */
  };

typedef struct Node
{
  /* Dubinski nivo */
  unsigned long level;
  /* Redni broj noda */
  unsigned long ord;
  /* Broj tela */
  unsigned long bc;

  /* Brojac cerki nodova */
  unsigned short dc;
  /* Poziciona oznaka noda (od 1 do 8, polozaj kockice u 
   * prostoru, 0 je root) */
  unsigned short npos;

  enum NodeType type;
  
  /* Clanovi razvoja potencijala noda - monopolni i *
   * kvadrupolni */
  double mono;
  double quad[6];

  /* Vektor geom. centra noda */
  Vector center;
  /* start - donji levi ugao, end - gornji desni ugao kockice */
  Vector start, end;

  Vector cm;

  struct Node *daughters[8];
  struct Node *prev, *next, *father;

  Body **bodies;
} Node;
										
typedef struct Tree 
{
  unsigned long levels;
  /* Ukupan broj nodova, praznih nodova i listova*/
  unsigned long nc, ec, lc;
  /* Pokazivac na sistem na kome se drvo gradi */
  NBody *sys;
  
  Node **nodes;
} Tree;

#define ND_SETFATHER(n,f) (n)->father = (f)
#define ND_SETNEXT(n,ne)  (n)->next   = (ne)
#define ND_SETPREV(n,p)   (n)->prev   = (p)

#define ND_SETDAUGHTER(np,cp)			\
  {						\
    (np)->daughters[(np)->dc] = (cp);		\
    (np)->dc++;					\
  }

#define ND_ADDBODY(np,bp)			\
  {						\
    (np)->bodies[(np)->bc] = bp;		\
    (np)->bc++;					\
  }

#define TREE_INIT(tp,np)			\
  {						\
    (tp) = XMALLOC( Tree, 1 );			\
    (tp)->sys    = (np);			\
    (np)->base   = (tp);			\
    (tp)->levels = 0;				\
    (tp)->nc     = 0;				\
    (tp)->lc     = 0;				\
    (tp)->ec     = 0;				\
    (tp)->nodes  = 0;				\
    buildTree( (tp) );				\
  }

#define TREE_FLUSH(tp) \
  {		       \
    flushTree( (tp) ); \
  }

/* Alocira memoriju za jedan nod i postavlja default vrednosti */
extern Node * makeNode( Vector *start, Vector *end, unsigned long level,
			unsigned long ord, unsigned short npos, unsigned long bc );

/* Izracunava vektor CM noda */
extern void calcNodeCM( Node* n );

extern void buildTree( Tree *t );

/* Smesta nodove pogodne za aproksimaciju o odnosu na tacku p u valid */
extern unsigned long scanTreeVector( Tree *t, Vector *p, Node *valid[] );

/* Smesta nodove pogodne za aproksimaciju o odnosu na telo b u valid */
extern unsigned long scanTreeBody( Tree *t, Body *b, Node *valid[] );

extern double calcPotTree( Tree *t );

extern Vector calcAccTree( Tree *t );

extern void flushTree( Tree *t );

#endif
