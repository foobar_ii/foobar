/*
    --------------------------------------------------------------------
    LICENSE:
    --------------------------------------------------------------------
    This file is part of Foo
    Copyright 2010 ISPAST Integratori Group
    --------------------------------------------------------------------
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    --------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __VECTOR_H__
#define __VECTOR_H__

#define VECT_SET(u,a,b,c)			\
  {		       				\
    (u).x = (a);				\
    (u).y = (b);				\
    (u).z = (c);				\
  }

#define VECT_NULLIFY(v) \
  {			\
    (v).x = 0.0;	\
    (v).y = 0.0;	\
    (v).z = 0.0;	\
  }

#define VECT_ADD(w,u,v)							\
  {									\
    (w).x = (u).x + (v).x;						\
    (w).y = (u).y + (v).y;						\
    (w).z = (u).z + (v).z;						\
  }

#define VECT_SUB(w,u,v)							\
  {									\
    (w).x = (u).x - (v).x;						\
    (w).y = (u).y - (v).y;						\
    (w).z = (u).z - (v).z;						\
  }

#define VECT_SMUL(w,u,s)						\
  {									\
    (w).x = (u).x * (s);						\
    (w).y = (u).y * (s);						\
    (w).z = (u).z * (s);						\
  }

#define VECT_CPY(u,v)							\
  {									\
    (u).x = (v).x;							\
    (u).y = (v).y;							\
    (u).z = (v).z;							\
  }

#define VECT_ABS(v) sqrt( ((v).x * (v).x) + ((v).y * (v).y) +	\
			  ((v).z * (v).z) )

#define VECT_DIST(u,v) sqrt( (((u).x-(v).x) * ((u).x-(v).x)) + \
			     (((u).y-(v).y) * ((u).y-(v).y)) + \
			     (((u).z-(v).z) * ((u).z-(v).z)) )

#define VECT_SCRPRT(v)						\
  {								\
    printf( "x:%.10lf y:%.10lf z:%.10lf", (v).x, (v).y, (v).z );	\
    fflush( stdout );						\
  }

typedef struct Vector
{
  double x, y, z;
} Vector;

/* 0 -> prvi veci, 2 -> drugi veci, 1 -> jednaki */
extern unsigned short compareVect( Vector *v1, Vector *v2 );

#endif
