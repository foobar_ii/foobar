/*
    --------------------------------------------------------------------
    LICENSE:
    --------------------------------------------------------------------
    This file is part of Foo
    Copyright 2010 ISPAST Integratori Group
    --------------------------------------------------------------------
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    --------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __TIMER_H__
#define __TIMER_H__

/* Nije u realnom vremenu! */
typedef struct Timer
{
  /* Pocetno vreme */
  double start_time;
  
  /* Trenutno vreme, osvezava se sa updateTimer i 
   * touchTimer */
  double timer_time;

  /* Prethodno vreme nakon merenja, 
   * osvezava se sa updateTimer */
  double last_time;
  
  /* Ukupnno proteklo vreme */
  double elapsed_time;
} Timer;


double getTime( void );

Timer * createTimer( void );

/* Poziva se pre onoga sto merimo, 
 * osim ako nije prvi put (tada se poziva createTimer) */
void touchTimer( Timer *t );

/* Poziva se nakon onoga sto merimo */
double updateTimer( Timer *t );

#endif
