/*
    --------------------------------------------------------------------
    LICENSE:
    --------------------------------------------------------------------
    This file is part of Foo
    Copyright 2010 ISPAST Integratori Group
    --------------------------------------------------------------------
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    --------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __COMMON_H__
#define __COMMON_H__

/** custom config include
  * Note: mainly for 'UNIX' builds
  */
#ifdef HAVE_CONFIG_H
#   include "config.h"
#endif

/** Define DEBUG according to _DEBUG.
  */
#if !defined( DEBUG ) && defined( _DEBUG )
#   define DEBUG 1
# endif /* !defined( DEBUG ) && defined( _DEBUG ) */

/** Define NDEBUG according to DEBUG; default to NDEBUG.
  */
#if defined( DEBUG )
#   undef NDEBUG
#elif !defined( NDEBUG )
#   define NDEBUG 1
#endif /* !defined( DEBUG ) && !defined( NDEBUG ) */

/** Build platform defines
  */
#if !defined( WIN32 ) && ( defined( __WIN32__ ) || defined( _WIN32 ) )
#   define WIN32 1
#endif /* !defined ( WIN32 ) && ( defined( __WIN32__ ) || defined( _WIN32 ) ) */

/** Visual Studio 'errors'/'warnings'
  */
#ifdef WIN32
#   ifdef _MSC_VER
#       define _CRT_SECURE_NO_WARNINGS 1
#       define _CRT_SECURE_NO_DEPRECATE 1
#       define _CRT_SECURE_COPP_OVERLOAD_STANDARD_NAMES 1
#       if DEBUG
#           define _CRTDBG_MAP_ALLOC 1
#       endif/*DEBUG*/
#       define _SCL_SECURE_NO_WARNINGS 1
#   endif/*_MSC_VER*/
#endif/*WIN32*/

/** Standard includes (platform independant)
  */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/** Platform dependent defines and includes
  */
#if WIN32
#   define WIN32_LEAN_AND_MEAN
#   define _WIN32_WINNT 0x0500
#   define NOMINMAX
#   include <windows.h>
/* Ko bi rekao, ali def. timeval strukture se nalazi u winsock2.h */
#   include <winsock2.h>
#   include <sys/timeb.h>
/* #   include <io.h> */
#else
#include <sys/time.h>
/* #   include <sys/types.h> */
/* #   include <sys/ioctl.h> */
#endif

/* Memorijski wrapperi iz libiberty-ja */
#define XMALLOC(type, num)			\
  ((type *) xmalloc ((num) * sizeof(type)))
#define XREALLOC(type, p, num)				\
  ((type *) xrealloc ((p), (num) * sizeof(type)))
#define XFREE(stale)					    \
  do {							    \
    if (stale) { free (stale); stale = 0; }		    \
  } while (0)

/* Ne treba za sada... */
enum ApproxMethod
  {
    NONE,
    TREECODE,
    PARTICLE_MESH
  };

#define PI 3.14159265358979323
#define G  1.0

#define FNAME_MAX_LEN 300

/** platform depended typedefs and prototypes
  */
#ifdef WIN32
extern int gettimeofday( struct timeval* tv, void* reserved );
#endif /* !WIN32 */

typedef unsigned short bool;

#define TRUE (bool) 1
#define FALSE (bool) 0

extern void *xmalloc   (size_t num);
extern void *xrealloc  (void *p, size_t num);

/* Broj tela */
extern unsigned long N;

/* Masa i dimenzije sistema */
extern double M, D;

/* Koef. aproksimacije nodova (treecode) */
extern double K;

/* Broj celija po dimenziji (mesh) */
extern unsigned int NUMCELLS;

#endif
